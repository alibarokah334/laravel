<?php

namespace App\Http\Controllers;

use Illuminate\http\Requests;
use Illuminate\Support\Facades\DB;


class siswaController extends BaseController
{
    public function index(){
        $siswa = DB::table('siswa')->get();
        return view('index',['siswa'=>$siswa]);
    }

}
